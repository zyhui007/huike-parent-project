package com.huike.domain.clues.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClueCountDTO {

    private String clueDate;
    private Integer clueCount;

}