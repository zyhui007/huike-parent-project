package com.huike.utils;

import com.huike.common.constant.MessageConstants;
import com.huike.common.exception.CustomException;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;

import javax.mail.internet.MimeMessage;

public class EmailUtils {

    private JavaMailSender javaMailSender;

    public EmailUtils(JavaMailSender javaMailSender) {
        this.javaMailSender = javaMailSender;
    }

    public void sendMailWithoutAttachment(String from, String to,  String subject, String text, String ... cc) throws Exception {
        if(StringUtils.isEmpty(from)) {
            throw new CustomException(MessageConstants.EMAIL_FROM_NOT_NULL);
        }
        if(StringUtils.isEmpty(to)){
            throw new CustomException(MessageConstants.EMAIL_TO_NOT_NULL);
        }

        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true);
        messageHelper.setFrom(from);//发件人
        messageHelper.setTo(to);//收件人
        if(cc != null && cc.length>0){
            messageHelper.setCc(cc);//抄送人
        }
        messageHelper.setSubject(subject);//主题
        messageHelper.setText(text , true);//内容
        javaMailSender.send(mimeMessage);
    }

}