package com.huike.utils.file;

import com.huike.common.config.MinioConfig;
import com.huike.utils.uuid.UUID;
import io.minio.*;
import io.minio.errors.MinioException;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

@Component
public class FileUploadByMinIO {
@Autowired
private MinioConfig minioConfig;


    public String upload(MultipartFile file) throws Exception {

                // Create a minioClient with the MinIO server playground, its access key and secret key.
        MinioClient minioClient =
        MinioClient.builder()
                            .endpoint("http://"+minioConfig.getEndpoint()+":"+minioConfig.getPort()) // http://127.0.0.1:9000
                            .credentials(minioConfig.getAccessKey(), minioConfig.getSecretKey())
                            .build();

        // Make 'asiatrip' bucket if not exist.
        boolean found =
        minioClient.bucketExists(BucketExistsArgs.builder().bucket(minioConfig.getBucketName()).build());
        if (!found) {
            // Make a new bucket called 'asiatrip'.
            minioClient.makeBucket(MakeBucketArgs.builder().bucket(minioConfig.getBucketName()).build());
        }
                String originalFilename = file.getOriginalFilename();
                String extName = originalFilename.substring(originalFilename.lastIndexOf("."));
                String objectName = UUID.randomUUID().toString()+extName;
                String foldername = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy/MM/"));
                objectName = foldername+objectName;
                // Upload '/home/user/Photos/asiaphotos.zip' as object name 'asiaphotos-2015.zip' to bucket
        // 'asiatrip'.
        minioClient.putObject(
        PutObjectArgs.builder()
                            .bucket(minioConfig.getBucketName())
                            .object(objectName)
                            .stream(file.getInputStream(),file.getSize(),-1)
                            .contentType(file.getContentType())
                            .build());



       return "/"+minioConfig.getBucketName()+"/"+objectName;
    }
}
