package com.huike.aspectj;

import com.huike.common.annotation.PreAuthorize;
import com.huike.domain.system.dto.LoginUser;
import com.huike.web.service.TokenService;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Set;

@Aspect
@Component
public class PermissionCheckAspect {
    @Autowired
    private TokenService tokenService;
    @Autowired
    private HttpServletRequest request;
    @Autowired
    private HttpServletResponse response;
    @Around("execution(* com.huike.controller.*.*.*(..)) && @annotation(preAuthorize)")
    public Object permissionCheck(ProceedingJoinPoint joinPoint ,PreAuthorize preAuthorize ) throws Throwable {
       LoginUser loginUser = tokenService.getLoginUser(request);

       if (loginUser == null){
        response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
        return null;
       }
        String value   =    preAuthorize.value();

        Set<String>  premissions = loginUser.getPermissions();

        if (!premissions.contains(value) && !premissions.contains("*:*:*")){
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return null;
        }

        return joinPoint.proceed();
    }
}
