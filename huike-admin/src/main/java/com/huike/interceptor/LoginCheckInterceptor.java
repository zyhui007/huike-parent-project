package com.huike.interceptor;

import com.huike.common.constant.HttpStatus;
import com.huike.domain.system.dto.LoginUser;
import com.huike.utils.StringUtils;
import com.huike.web.service.TokenService;
import com.sun.jna.platform.win32.Winnetwk;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@Component
public class LoginCheckInterceptor implements HandlerInterceptor {
    @Autowired
    private TokenService tokenService;

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler){
        String token = tokenService.getToken(request);

        if (StringUtils.isEmpty(token)){
            response.setStatus(HttpStatus.UNAUTHORIZED);
            return false;
        }

        try {
            tokenService.parseToken(token);
        } catch (Exception e) {
            e.printStackTrace();
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }

        LoginUser loginUser = tokenService.getLoginUser(request);
        if (loginUser == null){
            response.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            return false;
        }
        tokenService.refreshAndCacheToken(loginUser);
        return true;
    }
}
